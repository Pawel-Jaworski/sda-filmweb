package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.RateIdDto;
import pl.sda.sdafilmweb.domain.service.MovieService;
import pl.sda.sdafilmweb.domain.service.RateService;
import pl.sda.sdafilmweb.domain.service.UserService;

@Controller
@RequestMapping("rate")
@RequiredArgsConstructor
public class RateController {

    private final MovieService movieService;
    private final RateService rateService;
    private final UserService userService;

    @GetMapping("search")
    ModelAndView getAllRates() {
        ModelAndView modelAndView = new ModelAndView("searchRate.html");
        modelAndView.addObject("rates", rateService.getAll());
        modelAndView.addObject("movies", movieService.getAll());
        modelAndView.addObject("users", userService.getAll());
        modelAndView.addObject("actualUserName", SecurityContextHolder.getContext().getAuthentication().getName());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createModelView(@RequestParam Long movieId) {
        ModelAndView modelAndView = new ModelAndView("createRate.html");
        modelAndView.addObject("rate", new RateIdDto());
        modelAndView.addObject("movieId", movieId);
        return modelAndView;
    }

    @PostMapping("/create")
    String create(@ModelAttribute RateIdDto rateIdDto) {
        if (rateIdDto.getUserId() == null) {
            Long userId = userService.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
            rateIdDto.setUserId(userId);
        }
        rateService.createOrUpdate(rateIdDto);
        return "redirect:/";
    }

    @GetMapping("/edit")
    ModelAndView editRate(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("createRate.html");
        modelAndView.addObject("rate", rateService.getById(id));
        modelAndView.addObject("users", userService.getAll());
        modelAndView.addObject("movieId", rateService.getById(id).getMovieId());
        return modelAndView;
    }

    @GetMapping("/delete")
    String deleteRate(@RequestParam Long id) {
        rateService.delete(id);

        return "redirect:/";
    }
}
