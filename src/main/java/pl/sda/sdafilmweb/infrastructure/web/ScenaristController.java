package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.ScenaristDto;
import pl.sda.sdafilmweb.domain.service.ScenaristService;

@Controller
@RequestMapping("scenarist")
@RequiredArgsConstructor
public class ScenaristController {

    private final ScenaristService scenaristService;

    @GetMapping("/search")
    ModelAndView getAllScenarist() {
        ModelAndView modelAndView = new ModelAndView("searchScenarist.html");
        modelAndView.addObject("scenarists", scenaristService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createModelView() {
        ModelAndView modelAndView = new ModelAndView("createScenarist.html");
        modelAndView.addObject("scenarist", new ScenaristDto());
        return modelAndView;
    }

    @PostMapping("/create")
    String createScenarist(@ModelAttribute ScenaristDto scenaristDto) {
        scenaristService.createOrUpdate(scenaristDto);
        return "redirect:/";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/edit")
    ModelAndView editScenarist(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("createScenarist.html");
        modelAndView.addObject("scenarist", scenaristService.getById(id));
        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete")
    String deleteScenarist(@RequestParam Long id) {
        scenaristService.delete(id);
        return "rediect:/";
    }
}
