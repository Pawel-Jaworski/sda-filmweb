package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.DirectorDto;
import pl.sda.sdafilmweb.domain.service.DirectorService;

@Controller
@RequestMapping("director")
@RequiredArgsConstructor
public class DirectorController {

    private final DirectorService directorService;

    @GetMapping("/search")
    ModelAndView getAllDirectors() {
        ModelAndView modelAndView = new ModelAndView("searchDirector.html");
        modelAndView.addObject("directors", directorService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createModelView() {
        ModelAndView modelAndView = new ModelAndView("createDirector.html");
        modelAndView.addObject("director", new DirectorDto());
        return modelAndView;
    }

    @PostMapping("/create")
    String createDirector(@ModelAttribute DirectorDto directorDto) {
        directorService.createOrUpdate(directorDto);
        return "redirect:/";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/edit")
    ModelAndView editDirector(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("createDirector.html");
        modelAndView.addObject("director", directorService.getById(id));
        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete")
    String deleteDirector(@RequestParam Long id) {
        directorService.delete(id);
        return "redirect:/";
    }

}
