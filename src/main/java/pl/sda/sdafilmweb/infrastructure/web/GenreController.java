package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.GenreDto;
import pl.sda.sdafilmweb.domain.service.GenreService;

@Controller
@RequestMapping("genre")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @GetMapping("/search")
    ModelAndView getAllGenres() {
        ModelAndView modelAndView = new ModelAndView("searchGenre.html");
        modelAndView.addObject("genres", genreService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createGenreView() {
        ModelAndView modelAndView = new ModelAndView("createGenre.html");
        modelAndView.addObject("genre", new GenreDto());
        return modelAndView;
    }

    @PostMapping("/create")
    String createGenre(@ModelAttribute GenreDto genreDto) {
        genreService.createOrUpdate(genreDto);
        return "redirect:/";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/edit")
    ModelAndView editGenre(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("createGenre.html");
        modelAndView.addObject("genre", genreService.getById(id));
        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete")
    String deleteGenre(@RequestParam Long id) {
        genreService.delete(id);
        return "redirect:/";
    }
}
