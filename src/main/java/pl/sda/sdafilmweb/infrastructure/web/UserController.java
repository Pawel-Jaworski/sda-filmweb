package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.UserDto;
import pl.sda.sdafilmweb.domain.service.UserService;

@Controller
@RequestMapping("user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/search")
    ModelAndView getAllUsers() {
        ModelAndView modelAndView = new ModelAndView("searchUser.html");
        modelAndView.addObject("users", userService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createUserView() {
        ModelAndView modelAndView = new ModelAndView(("register.html"));
        modelAndView.addObject("user", new UserDto());
        return modelAndView;
    }

    @PostMapping("/create")
    String createUser(@ModelAttribute UserDto userDto) {
        userService.create(userDto);
        return "redirect:/";
    }
}
