package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.MovieIdDto;
import pl.sda.sdafilmweb.domain.service.*;

@Controller
@RequestMapping("movie")
@RequiredArgsConstructor
@Slf4j
public class MovieController {

    private final ActorService actorService;
    private final DirectorService directorService;
    private final GenreService genreService;
    private final MovieService movieService;
    private final ScenaristService scenaristService;
    private final UserService userService;

    @GetMapping("search")
    ModelAndView getAllMovies() {
        ModelAndView modelAndView = new ModelAndView("searchMovie.html");
        modelAndView.addObject("movies", movieService.getAll());
        modelAndView.addObject("scenarists", scenaristService.getAll());
        modelAndView.addObject("genres", genreService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createMovieView() {
        ModelAndView modelAndView = new ModelAndView("createMovie.html");
        modelAndView.addObject("movieDto", new MovieIdDto());
        modelAndView.addObject("actors", actorService.getAll());
        modelAndView.addObject("directors", directorService.getAll());
        modelAndView.addObject("genres", genreService.getAll());
        modelAndView.addObject("scenarists", scenaristService.getAll());
        modelAndView.addObject("users", userService.getAll());
        return modelAndView;
    }

    @PostMapping("/create")
    String createMovie(@ModelAttribute MovieIdDto movieIdDto) {

        log.info(movieIdDto.toString());
        movieService.create(movieIdDto);
        return "redirect:/";
    }

    @GetMapping("/delete")
    String deleteMovie(@RequestParam Long id) {
        log.info(id.toString());
        movieService.delete(id);

        return "redirect:/";
    }

}
