package pl.sda.sdafilmweb.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.sdafilmweb.domain.dto.ActorDto;
import pl.sda.sdafilmweb.domain.service.ActorService;

@Controller
@RequestMapping("actor")
@RequiredArgsConstructor
public class ActorController {

    private final ActorService actorService;

    @GetMapping("/search")
    ModelAndView getAllActors() {
        ModelAndView modelAndView = new ModelAndView("searchActor.html");
        modelAndView.addObject("actors", actorService.getAll());
        return modelAndView;
    }

    @GetMapping("/create")
    ModelAndView createActorView() {
        ModelAndView modelAndView = new ModelAndView("createActor.html");
        modelAndView.addObject("actor", new ActorDto());
        return modelAndView;
    }

    @PostMapping("/create")
    String createActor(@ModelAttribute ActorDto actorDto) {
        actorService.createorUpdate(actorDto);
        return "redirect:/";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/edit")
    ModelAndView editActor(@RequestParam Long id) {
        ModelAndView modelAndView = new ModelAndView("createActor.html");
        modelAndView.addObject("actor", actorService.getById(id));
        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete")
    String deleteActor(@RequestParam Long id) {
        actorService.delete(id);
        return "redirect:/";
    }
}
