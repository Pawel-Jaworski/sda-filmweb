package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.GenreDto;
import pl.sda.sdafilmweb.domain.entity.Genre;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.repository.GenreRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GenreService {

    private final GenreRepository genreRepository;

    public Set<GenreDto> getAll() {
        return genreRepository.findAll()
                .stream()
                .map(Genre::toDto)
                .collect(Collectors.toSet());
    }

    public GenreDto getById(Long id) {
        return genreRepository.findById(id).get().toDto();
    }

    public void createOrUpdate(GenreDto genreDto) {
        Genre genre = Genre.builder()
                .name(genreDto.getName())
                .movies(new HashSet<Movie>())
                .build();
        genreRepository.save(genre);
    }

    public void delete(Long id) {
        genreRepository.deleteById(id);
    }
}
