package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.ScenaristDto;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.entity.Scenarist;
import pl.sda.sdafilmweb.domain.repository.ScenaristRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScenaristService {

    private final ScenaristRepository scenaristRepository;

    public Set<ScenaristDto> getAll() {
        return scenaristRepository.findAll()
                .stream()
                .map(Scenarist::toDto)
                .collect(Collectors.toSet());
    }

    public ScenaristDto getById(Long id) {
        return scenaristRepository.findById(id).get().toDto();
    }

    public void createOrUpdate(ScenaristDto scenaristDto) {
        Scenarist scenarist = Scenarist.builder()
                .id(scenaristDto.getId())
                .firstname(scenaristDto.getFirstname())
                .surname(scenaristDto.getSurname())
                .movies(new HashSet<Movie>())
                .build();
        scenaristRepository.save(scenarist);
    }

    public void delete(Long id) {
        scenaristRepository.deleteById(id);
    }
}
