package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.RateDto;
import pl.sda.sdafilmweb.domain.dto.RateIdDto;
import pl.sda.sdafilmweb.domain.entity.Rate;
import pl.sda.sdafilmweb.domain.entity.User;
import pl.sda.sdafilmweb.domain.repository.MovieRepository;
import pl.sda.sdafilmweb.domain.repository.RateRepository;
import pl.sda.sdafilmweb.domain.repository.UserRepository;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class RateService {

    private final MovieRepository movieRepository;
    private final RateRepository rateRepository;
    private final UserRepository userRepository;


    public Set<RateDto> getAll() {
        return rateRepository.findAll()
                .stream()
                .map(Rate::toDto)
                .collect(Collectors.toSet());
    }

    public void createOrUpdate(RateIdDto rateIdDto) {
        Rate rate = Rate.builder()
                .id(rateIdDto.getId())
                .movie(movieRepository.findById(rateIdDto.getMovieId()).get())
                .user(userRepository.findById(rateIdDto.getUserId()).get())
                .numericalRate(rateIdDto.getNumericalRate())
                .description(rateIdDto.getDescription())
                .build();
        User actualUser = userRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        if (rate.getId() != null) {
            if (!rate.getUser().getId().equals(actualUser.getId())) {
                throw new AuthorizationServiceException("No permission to update this rate");
            }

        }
        rateRepository.save(rate);
    }

    public void delete(Long id) {
        Rate rateForDelete = rateRepository.findById(id).get();
        User actualUser = userRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        if (!rateForDelete.getUser().getId().equals(actualUser.getId())) {
            Set admins = actualUser.getRoles().stream().filter(userRole -> userRole.getId() == 2).collect(Collectors.toSet());
            if (admins.isEmpty()) {
                throw new AuthorizationServiceException("No permission to remove this rate");
            }
        }
        rateRepository.deleteById(id);
    }

    public RateIdDto getById(Long id) {
        return rateRepository.findById(id).get().toIdDto();
    }
}
