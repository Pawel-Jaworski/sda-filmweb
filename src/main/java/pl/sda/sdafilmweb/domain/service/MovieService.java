package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.MovieDto;
import pl.sda.sdafilmweb.domain.dto.MovieIdDto;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.entity.Rate;
import pl.sda.sdafilmweb.domain.repository.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class MovieService {

    private final MovieRepository movieRepository;
    private final DirectorRepository directorRepository;
    private final UserRepository userRepository;
    private final ScenaristRepository scenaristRepository;
    private final GenreRepository genreRepository;
    private final ActorRepository actorRepository;

    public void create(MovieIdDto movieIdDto) {
        Movie movie = Movie.builder()
                .title(movieIdDto.getTitle())
                .year(movieIdDto.getYear())
                .rates(new HashSet<Rate>())
                .director(directorRepository.findById(movieIdDto.getDirectorId()).get())
                .addedBy(userRepository.findById(movieIdDto.getAddedById()).get())
                .scenarists(new HashSet<>(scenaristRepository.findAllById(Arrays.asList(movieIdDto.getScenaristsIds()))))
                .genres(new HashSet<>(genreRepository.findAllById(Arrays.asList(movieIdDto.getGenresIds()))))
                .actors(new HashSet<>(actorRepository.findAllById(Arrays.asList(movieIdDto.getActorsIds()))))
                .build();
        movieRepository.save(movie);
    }

    public Set<MovieDto> getAll() {
        return movieRepository.findAll()
                .stream()
                .map(Movie::toDto)
                .collect(Collectors.toSet());
    }

    public void delete(Long movieId) {
        movieRepository.deleteById(movieId);
    }
}
