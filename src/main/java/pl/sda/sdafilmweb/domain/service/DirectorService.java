package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.DirectorDto;
import pl.sda.sdafilmweb.domain.entity.Director;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.repository.DirectorRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DirectorService {

    private final DirectorRepository directorRepository;

    public Set<DirectorDto> getAll() {
        return directorRepository.findAll()
                .stream()
                .map(Director::toDto)
                .collect(Collectors.toSet());
    }

    public DirectorDto getById(Long id) {
        return directorRepository.findById(id).get().toDto();
    }

    public void createOrUpdate(DirectorDto directorDto) {
        Director director = Director.builder()
                .id(directorDto.getId())
                .firstname(directorDto.getFirstname())
                .surname(directorDto.getSurname())
                .movies(new HashSet<Movie>())
                .build();
        directorRepository.save(director);
    }

    public void delete(Long id) {
        directorRepository.deleteById(id);
    }
}
