package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.ActorDto;
import pl.sda.sdafilmweb.domain.entity.Actor;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.repository.ActorRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActorService {

    private final ActorRepository actorRepository;

    public Set<ActorDto> getAll() {
        return actorRepository.findAll()
                .stream()
                .map(Actor::toDto)
                .collect(Collectors.toSet());
    }

    public ActorDto getById(Long id) {
        return actorRepository.findById(id).get().toDto();
    }

    public void createorUpdate(ActorDto actorDto) {
        Actor actor = Actor.builder()
                .id(actorDto.getId())
                .firstname(actorDto.getFirstname())
                .surname(actorDto.getSurname())
                .movies(new HashSet<Movie>())
                .build();
        actorRepository.save(actor);
    }

    public void delete(Long id) {
        actorRepository.deleteById(id);
    }
}
