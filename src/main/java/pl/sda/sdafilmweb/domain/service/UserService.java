package pl.sda.sdafilmweb.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.sdafilmweb.domain.dto.UserDto;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.entity.Rate;
import pl.sda.sdafilmweb.domain.entity.User;
import pl.sda.sdafilmweb.domain.repository.UserRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Set<UserDto> getAll() {
        return userRepository.findAll()
                .stream()
                .map(User::toDto)
                .collect(Collectors.toSet());
    }

    public UserDto getByLogin(String login) {
        return userRepository.findByLogin(login).toDto();
    }

    public void create(UserDto userDto) {
        User user = User.builder()
                .login(userDto.getLogin())
                .password(userDto.getPassword())
                .rates(new HashSet<Rate>())
                .addedMovies(new HashSet<Movie>())
                .build();
        userRepository.save(user);
    }
}
