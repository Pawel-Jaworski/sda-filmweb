package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.UserDto;

import javax.persistence.*;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String login;
    private String password;
    @OneToMany(mappedBy = "user")
    private Set<Rate> rates;
    @OneToMany(mappedBy = "addedBy")
    private Set<Movie> addedMovies;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<UserRole> roles;

    public UserDto toDto() {
        return UserDto.builder()
                .id(id)
                .login(login)
                .password(password)
                .build();
    }
}
