package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.DirectorDto;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "directors")
public class Director {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String surname;
    @OneToMany(mappedBy = "director")
    private Set<Movie> movies;

    public DirectorDto toDto() {
        return DirectorDto.builder()
                .id(id)
                .firstname(firstname)
                .surname(surname)
                .build();
    }
}
