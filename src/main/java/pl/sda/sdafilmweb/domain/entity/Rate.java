package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.RateDto;
import pl.sda.sdafilmweb.domain.dto.RateIdDto;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "rate")
public class Rate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Movie movie;
    private Integer numericalRate;
    private String description;

    public RateDto toDto() {
        return RateDto.builder()
                .id(id)
                .user(user)
                .movie(movie)
                .numericalRate(numericalRate)
                .description(description)
                .build();
    }

    public RateIdDto toIdDto() {
        return RateIdDto.builder()
                .id(id)
                .userId(user.getId())
                .movieId(movie.getId())
                .numericalRate(numericalRate)
                .description(description)
                .build();
    }
}
