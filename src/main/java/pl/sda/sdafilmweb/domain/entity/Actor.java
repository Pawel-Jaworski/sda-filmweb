package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.ActorDto;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "actors")
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String surname;
    @ManyToMany(mappedBy = "actors")
    Set<Movie> movies;

    public ActorDto toDto() {
        return ActorDto.builder()
                .id(id)
                .firstname(firstname)
                .surname(surname)
                .movies(movies)
                .build();
    }
}
