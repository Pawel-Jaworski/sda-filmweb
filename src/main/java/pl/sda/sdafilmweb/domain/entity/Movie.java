package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.MovieDto;
import pl.sda.sdafilmweb.domain.dto.MovieIdDto;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private Integer year;
    @OneToMany(mappedBy = "movie")
    private Set<Rate> rates;
    @ManyToOne
    private Director director;
    @ManyToOne
    private User addedBy;
    @ManyToMany
    @JoinTable(name = "movie_scenarist",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "scenarist_id")})
    private Set<Scenarist> scenarists;
    @ManyToMany
    @JoinTable(name = "movie_genre",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")})
    private Set<Genre> genres;
    @ManyToMany
    @JoinTable(name = "movie_actor",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    private Set<Actor> actors;

    public MovieIdDto toIdDto() {
        return MovieIdDto.builder()
                .id(id)
                .title(title)
                .year(year)
                .directorId(director.getId())
                .scenaristsIds(scenarists.stream().map(Scenarist::getId).toArray(Long[]::new))
                .genresIds(genres.stream().map(Genre::getId).toArray(Long[]::new))
                .actorsIds(actors.stream().map(Actor::getId).toArray(Long[]::new))
                .build();
    }

    public MovieDto toDto() {
        return MovieDto.builder()
                .id(id)
                .title(title)
                .year(year)
                .rates(rates)
                .director(director)
                .scenarists(scenarists)
                .genres(genres)
                .actors(actors)
                .build();
    }

    {
    }
}
