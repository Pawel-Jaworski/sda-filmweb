package pl.sda.sdafilmweb.domain.entity;

import lombok.*;
import pl.sda.sdafilmweb.domain.dto.ScenaristDto;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "scenarists")
public class Scenarist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String surname;
    @ManyToMany(mappedBy = "scenarists")
    Set<Movie> movies;

    public ScenaristDto toDto() {
        return ScenaristDto.builder()
                .id(id)
                .firstname(firstname)
                .surname(surname)
                .build();
    }
}
