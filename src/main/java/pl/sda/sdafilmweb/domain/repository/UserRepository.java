package pl.sda.sdafilmweb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sdafilmweb.domain.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);
}
