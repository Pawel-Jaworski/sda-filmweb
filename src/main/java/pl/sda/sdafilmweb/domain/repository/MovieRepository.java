package pl.sda.sdafilmweb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sdafilmweb.domain.entity.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
