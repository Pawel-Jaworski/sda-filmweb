package pl.sda.sdafilmweb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sdafilmweb.domain.entity.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {

}
