package pl.sda.sdafilmweb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sdafilmweb.domain.entity.Rate;

public interface RateRepository extends JpaRepository<Rate, Long> {

}
