package pl.sda.sdafilmweb.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.sdafilmweb.domain.entity.Actor;

public interface ActorRepository extends JpaRepository<Actor, Long> {

}
