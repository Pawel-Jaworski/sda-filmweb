package pl.sda.sdafilmweb.domain.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RateIdDto {

    private Long id;
    private Long userId;
    private Long movieId;
    private Integer numericalRate;
    private String description;
}
