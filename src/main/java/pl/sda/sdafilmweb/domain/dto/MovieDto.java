package pl.sda.sdafilmweb.domain.dto;

import lombok.*;
import pl.sda.sdafilmweb.domain.entity.*;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MovieDto {

    private Long id;
    private String title;
    private Integer year;
    private Set<Rate> rates;
    private Director director;
    private User addedBy;
    private Set<Scenarist> scenarists;
    private Set<Genre> genres;
    private Set<Actor> actors;

    public Float getAverageRate() {

        Integer sum = 0;

        for (Rate rate : rates) {
            sum += rate.getNumericalRate();
        }

        return (float) sum / rates.size();
    }
}
