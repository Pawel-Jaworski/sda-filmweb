package pl.sda.sdafilmweb.domain.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MovieIdDto {

    private Long id;
    private String title;
    private Integer year;
    private Long directorId;
    private Long addedById;
    private Long[] scenaristsIds;
    private Long[] genresIds;
    private Long[] actorsIds;
}
