package pl.sda.sdafilmweb.domain.dto;

import lombok.*;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.entity.User;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RateDto {

    private Long id;
    private User user;
    private Movie movie;
    private Integer numericalRate;
    private String description;
}
