package pl.sda.sdafilmweb.domain.dto;

import lombok.*;
import pl.sda.sdafilmweb.domain.entity.Movie;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActorDto {

    private Long id;
    private String firstname;
    private String surname;
    Set<Movie> movies;
}
