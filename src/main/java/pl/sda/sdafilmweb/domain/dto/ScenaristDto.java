package pl.sda.sdafilmweb.domain.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScenaristDto {

    private Long id;
    private String firstname;
    private String surname;
}
