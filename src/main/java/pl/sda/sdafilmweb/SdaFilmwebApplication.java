package pl.sda.sdafilmweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SdaFilmwebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SdaFilmwebApplication.class, args);
    }

}
