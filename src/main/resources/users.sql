INSERT INTO roles (id, role)
VALUES (1, 'ROLE_USER');
INSERT INTO users (id, login, password)
VALUES (1, 'user', 'user');
INSERT INTO user_role (user_id, role_id)
VALUES (1, 1);

INSERT INTO roles (id, role)
VALUES (2, 'ROLE_ADMIN');
INSERT INTO users (id, login, password)
VALUES (2, 'admin', 'admin');
INSERT INTO user_role (user_id, role_id)
VALUES (2, 1);
INSERT INTO user_role (user_id, role_id)
VALUES (2, 2);