package pl.sda.sdafilmweb.infrastructure.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ActorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnAllActors() throws Exception {
        mockMvc
                .perform(get("/actor/search"))
                .andExpect(status().isOk());
    }

    @Test
    public void createActorView() {
    }

    @Test
    public void createActor() {
    }
}
