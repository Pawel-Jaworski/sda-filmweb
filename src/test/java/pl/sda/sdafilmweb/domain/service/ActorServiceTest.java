package pl.sda.sdafilmweb.domain.service;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import pl.sda.sdafilmweb.domain.dto.ActorDto;
import pl.sda.sdafilmweb.domain.entity.Actor;
import pl.sda.sdafilmweb.domain.entity.Movie;
import pl.sda.sdafilmweb.domain.repository.ActorRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ActorServiceTest {

    private ActorRepository actorRepository = Mockito.mock(ActorRepository.class);
    private ArgumentCaptor<Actor> actorCaptor = ArgumentCaptor.forClass(Actor.class);
    private ActorService actorService = new ActorService(actorRepository);

    @Test
    public void shouldReturnAllActors() {
        //given
        List<Actor> actors = Arrays.asList(
                Actor.builder().id(1L).firstname("Cezary").surname("Pazura").movies(new HashSet<Movie>()).build(),
                Actor.builder().id(1L).firstname("Olaf").surname("Lubaszenko").movies(new HashSet<Movie>()).build(),
                Actor.builder().id(1L).firstname("Małgorzata").surname("Kożuchowska").movies(new HashSet<Movie>()).build()
        );
        Mockito.when(actorRepository.findAll()).thenReturn(actors);

        //when
        Set<ActorDto> actorDtos = actorService.getAll();

        //then
        assertEquals(3, actorDtos.size());
    }

    @Test
    public void shouldCreateActor() {

        //given
        ActorDto actorDto = ActorDto.builder().id(1L).firstname("Janusz").surname("Rewiński").movies(new HashSet<Movie>()).build();
        //when
        actorService.createorUpdate(actorDto);

        //then

        Mockito.verify(actorRepository, Mockito.times(1)).save(actorCaptor.capture());

        Actor capturedActor = actorCaptor.getValue();
        assertEquals("Janusz", capturedActor.getFirstname());
        assertEquals("Rewiński", capturedActor.getSurname());
        assertEquals(new HashSet<Movie>(), capturedActor.getMovies());

    }

    @Test
    public void shouldUpdateActor() {

        //given
        ActorDto actorDto = ActorDto.builder().id(2L).firstname("Cezary").surname("Pazura").movies(new HashSet<Movie>()).build();
        ActorDto updateActorDto = ActorDto.builder().id(2L).firstname("Radosław").surname("Pazura").movies(new HashSet<Movie>()).build();
        actorService.createorUpdate(actorDto);

        //when
        actorService.createorUpdate(updateActorDto);

        //then


        Actor capturedActor = actorCaptor.getValue();
    }
}
